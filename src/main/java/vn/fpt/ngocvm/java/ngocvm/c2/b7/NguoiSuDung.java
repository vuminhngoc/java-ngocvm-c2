/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vn.fpt.ngocvm.java.ngocvm.c2.b7;

import java.util.ArrayList;

/**
 *
 * @author ngocvm
 */
public class NguoiSuDung{
    int[] user = new int[200];
    
    public int getRandomUser(IRandomer rand){
        return user[rand.getRandomNumber(200)];
    }
    
    
    public static void main(String[] args) {
        NguoiSuDung auser = new NguoiSuDung();
        IRandomer rand = new UseClassRandom();
        System.out.println(auser.getRandomUser(rand));
    }
}
