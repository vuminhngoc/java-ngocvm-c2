/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vn.fpt.ngocvm.java.ngocvm.c2.b7;

/**
 *
 * @author ngocvm
 */
public class SystemTimeRandom implements IRandomer{

    @Override
    public int getRandomNumber(int maxValue) {
        return (int) (System.currentTimeMillis() % maxValue);
    }
    
}
