/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vn.fpt.ngocvm.java.ngocvm.c2.b6;

/**
 *
 * @author User-Admin
 */
public class NhanVienBanHang extends NhanVienVanPhong implements WorkBenefit{
 
    public NhanVienBanHang(String totNghiepTruong, String queQuan, int maNhanVien, String tenNhanVien, String ngaySinh) {
        super(totNghiepTruong, queQuan, maNhanVien, tenNhanVien, ngaySinh);
    }

    @Override
    public String getWorkBenefit() {
        return "Chi tra tien an trua va dien thoai";
    }
     
}
