/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vn.fpt.ngocvm.java.ngocvm.c2.b9;

import java.util.ArrayList;
import java.util.List;
import vn.fpt.ngocvm.java.ngocvm.c2.b6.BaoVe;

/**
 *
 * @author ngocvm
 */
public class DanhSachDoiTuong {
    public static void main(String[] args) {
        BaoVe baoVeNhat = new BaoVe("Dai den", 2, "Vo Van Nhat", "01/07/1988");
        BaoVe baoVeQuyet = new BaoVe("Dai xanh", 3, "Tran Van Quyet", "01/07/1978");
        BaoVe baoVeBach = new BaoVe("Dai do", 7, "Tran Xuan Bach", "07/07/200");
        
        List<BaoVe> danhSachBaoVe = new ArrayList<>(3);
        danhSachBaoVe.add(baoVeBach);
        danhSachBaoVe.add(baoVeQuyet);
        danhSachBaoVe.add(baoVeNhat);
        
        
        BaoVe baoVeNhatX = baoVeNhat;
        BaoVe baoVeNhat2 = new BaoVe("Dai den", 2, "Vo Van Nhat", "01/07/1988");
        System.out.println("gia tri 1 = "+ baoVeNhat.toString());
        System.out.println("gia tri 2 = "+ baoVeNhat2);
        System.out.println("gia tri X = "+ baoVeNhatX);
        System.out.println("Gia tri 1 == gia tri 2:"+ (baoVeNhat2.equals(baoVeNhat)));

        
        if(danhSachBaoVe.contains(baoVeNhat2))
            System.out.println("Bao ve Nhat co nam trong danh sach");
        else
            System.out.println("Bao ve Nhat khong nam trong danh sach");
        
    }
}
