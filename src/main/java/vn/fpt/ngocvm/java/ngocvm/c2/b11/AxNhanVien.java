/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vn.fpt.ngocvm.java.ngocvm.c2.b11;

import java.util.HashMap;
import java.util.Map;
import vn.fpt.ngocvm.java.ngocvm.c2.b6.BaoVe;
import vn.fpt.ngocvm.java.ngocvm.c2.b6.NhanVien;
import vn.fpt.ngocvm.java.ngocvm.c2.b6.NhanVienBanHang;

/**
 *
 * @author ngocvm
 */
public class AxNhanVien {
    public static void main(String[] args) {
        // Bài tập hashmap
        Map<Integer, NhanVien> axNhanVien = new HashMap<>();
        
        BaoVe bv = new BaoVe("dai den", 5, 
                "Nguyen Thanh Long", "20/10/2000");
        
        NhanVien nvbh = new NhanVienBanHang("FPT", "Hanoi", 
                7, "Nguyen Van Khoa", "12/06/2010");
        
        axNhanVien.put(bv.getMaNhanVien(), bv);
        axNhanVien.put(nvbh.getMaNhanVien(), nvbh);
        
        System.out.println("keyset=" + axNhanVien.keySet());
        System.out.println("keyset=" + axNhanVien.values());
        
        NhanVien nvGetFromMap = axNhanVien.get(7);
        System.out.println("Ten Nhan vien:"+nvGetFromMap.getTenNhanVien());
        
        
        NhanVien nvGetFromDelete = axNhanVien.remove(7);
        System.out.println("Ten Nhan vien deleted:"+nvGetFromDelete.getTenNhanVien());
        System.out.println("Keyset = "+ axNhanVien.keySet());
        
        axNhanVien.forEach((k, v)-> System.out.println("key="+k+"value ="+v));
        
    }
}
