/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vn.fpt.ngocvm.java.ngocvm.c2.b7;

import java.util.Random;

/**
 *
 * @author ngocvm
 */
public class UseClassRandom implements IRandomer{

    @Override
    public int getRandomNumber(int maxValue) {
        Random rand = new Random();
        return rand.nextInt(maxValue);
    }
    
    public int getMaxValue(){
        return 0;
    }
}
