/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vn.fpt.ngocvm.java.ngocvm.c2.b6;

/**
 *
 * @author User-Admin
 */
public class QuanLyBanHang extends NhanVienVanPhong implements WorkBenefit{
    private String chungChiNghe;

    public QuanLyBanHang(String chungChiNghe, String totNghiepTruong, String queQuan, int maNhanVien, String tenNhanVien, String ngaySinh) {
        super(totNghiepTruong, queQuan, maNhanVien, tenNhanVien, ngaySinh);
        this.chungChiNghe = chungChiNghe;
    }

    public String getChungChiNghe() {
        return chungChiNghe;
    }

    public void setChungChiNghe(String chungChiNghe) {
        this.chungChiNghe = chungChiNghe;
    }

    @Override
    public String getWorkBenefit() {
        return "Chi tra chi phi di lai.";
    }
    
}
