/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vn.fpt.ngocvm.java.ngocvm.c2.b11;

import java.util.HashMap;
import java.util.Map;
import vn.fpt.ngocvm.java.ngocvm.c2.b6.BaoVe;
import vn.fpt.ngocvm.java.ngocvm.c2.b6.NhanVien;
import vn.fpt.ngocvm.java.ngocvm.c2.b6.NhanVienBanHang;

/**
 *
 * @author User-Admin
 */
public class PracticeMap {

    public static void main(String[] args) {
        // Khởi tạo một đối tượng có kiểu Map<K,V>, và đối tượng được khởi tạo 
        // bằng class HashMap với số phần tử ban đầu bằng 10
        Map<String, String> tuDienAnhViet = new HashMap<>(10);

        // Khởi tạo một Map mới, bằng cách copy toàn bộ dữ liệu của Map từ điển
        // anh việt có sẵn
        Map<String, String> tuDienAnhViet2 = new HashMap<>(tuDienAnhViet);

        // Khởi tạo một từ điển nhân viên với Key là mã nhân viên, Value là
        // một đối tượng NhanVien nhằm truy vấn nhanh thông tin nhân viên
        // theo mã nhân viên
        Map<Integer, NhanVien> danhSachNhanVien = new HashMap<>();

        // Đẩy các cặp key-value vào trong map
        tuDienAnhViet.put("family", "Gia đình");
        tuDienAnhViet.put("love", "Yêu");
        tuDienAnhViet.put("love", "Tình Yêu");

        // Kiểm tra sự tồn tại của một key nào đó ở trong map
        boolean coChuaFamily = tuDienAnhViet.containsKey("family");

        if (tuDienAnhViet.containsKey("family")) {
            System.out.println("Từ điển có chứa từ Family.");
        }

        // cách lấy giá trị từ trong map
        String nghiaLove = tuDienAnhViet.get("love");
        System.out.println("Nghĩa của từ Love : " + nghiaLove);
        String nghiaHate = tuDienAnhViet.get("hate");
        System.out.println("Nghĩa của từ Hate : " + nghiaHate);
//        System.out.println("Null exception : " + danhSachNhanVien.get(123).getTenNhanVien());

        // Xóa giá trị khỏi map
        String nghiaBiXoa = tuDienAnhViet.remove("love");

        danhSachNhanVien.remove(23);

        // Duyệt qua các phần tử của hashmap sử dụng chuyển đổi về cặp Hashset
        for (Map.Entry<String, String> entry : tuDienAnhViet.entrySet()) {
            System.out.println("key=" + entry.getKey() + ", value=" + entry.getValue());
        }

        // Duyệt các phần tử thông qua key set
        for (String key : tuDienAnhViet.keySet()) {
            System.out.println("key=" + key + ", value=" + tuDienAnhViet.get(key));
        }

        // Duyệt sử dụng biểu thức lambda
        tuDienAnhViet.forEach((key, value) 
                -> System.out.println("Key=" + key + ", Value =" + value));
        
        
        
        // Bài tập hashmap
        Map<Integer, NhanVien> axNhanVien = new HashMap<>();
        
        BaoVe bv = new BaoVe("dai den", 5, 
                "Nguyen Thanh Long", "20/10/2000");
        
        NhanVien nvbh = new NhanVienBanHang("FPT", "Hanoi", 
                7, "Nguyen Van Khoa", "12/06/2010");
        
        axNhanVien.put(bv.getMaNhanVien(), bv);
        axNhanVien.put(nvbh.getMaNhanVien(), nvbh);
        
        System.out.println("keyset=" + axNhanVien.keySet());
    }
}
