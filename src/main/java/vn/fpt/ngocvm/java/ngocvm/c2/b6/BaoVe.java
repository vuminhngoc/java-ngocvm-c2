/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vn.fpt.ngocvm.java.ngocvm.c2.b6;

import java.util.Objects;

/**
 *
 * @author User-Admin
 */
public class BaoVe extends NhanVien implements WorkBenefit{
    private String chungChiVoNghe;

    public BaoVe(String chungChiVoNghe, int maNhanVien, String tenNhanVien, String ngaySinh) {
        super(maNhanVien, tenNhanVien, ngaySinh);
        this.chungChiVoNghe = chungChiVoNghe;
    }

    public void setChungChiVoNghe(String chungChiVoNghe) {
        this.chungChiVoNghe = chungChiVoNghe;
    }

    public String getChungChiVoNghe() {
        return chungChiVoNghe;
    }

    @Override
    public String toString() {
        return "BaoVe{" + "chungChiVoNghe=" + chungChiVoNghe + '}';
    }

    @Override
    public String getWorkBenefit() {
        return "Chi tra tien an trua va toi.";
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 43 * hash + Objects.hashCode(this.chungChiVoNghe);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final BaoVe other = (BaoVe) obj;
        return Objects.equals(this.chungChiVoNghe, other.chungChiVoNghe);
    }
    
    
    
}
