/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vn.fpt.ngocvm.java.ngocvm.c2.b9;

import java.util.ArrayList;
import java.util.List;
import vn.fpt.ngocvm.java.ngocvm.c2.b6.BaoVe;
import vn.fpt.ngocvm.java.ngocvm.c2.b6.NhanVien;
import vn.fpt.ngocvm.java.ngocvm.c2.b6.WorkBenefit;

/**
 *
 * @author User-Admin
 */
public class ExampleArrayList {
    public static void main(String[] args) {
        // Khởi tạo ArrayList danhSachMaNhanVien với kích thước ban đầu là 500
        // và chỉ chứa các số nguyên
        List<Integer> danhSachMaNhanVien = new ArrayList<>(500);
        // Khởi tạo ArrayList danhSachBaoVe, là một danh sách với kiểu là đối 
        // tượng thuộc class BaoVe
        List<BaoVe> danhSachBaoVe = new ArrayList<>();
        // Khởi tạo ArrayList danhSachNhanVien, là một danh sách với kiểu là đối 
        // tượng thuộc class cha NhanVien. Dữ liệu được lấy từ danhSachBaoVe
        List<NhanVien> danhSachNhanVien = new ArrayList<>(danhSachBaoVe);
        // Khởi tạo ArrayList danhSachWorkBenefit, là một danh sách với kiểu là đối 
        // tượng thuộc interface WorkBenefit. Dữ liệu được lấy từ danhSachBaoVe
        List<WorkBenefit> danhSachWorkBenefit = new ArrayList<>(danhSachBaoVe);
        
        danhSachMaNhanVien.add(1);
        int maNhanVienA = 56;
        danhSachMaNhanVien.add(maNhanVienA);
        
        BaoVe nhatBaoVe = new BaoVe("Dai Den taekwondo", 2, "Nguyễn Văn Nhật", "10/10/1977");
        danhSachBaoVe.add(nhatBaoVe);
        danhSachNhanVien.add(nhatBaoVe);
        danhSachWorkBenefit.add(nhatBaoVe);
        
        int maNhanVien = danhSachMaNhanVien.get(5);
        BaoVe baoVe = danhSachBaoVe.get(0);
        NhanVien nhanVien = danhSachNhanVien.get(1);
        WorkBenefit benefit = danhSachWorkBenefit.get(maNhanVien);    
        
        BaoVe longBaoVe = new BaoVe("Dai Do taekwondo", 3, "Nguyễn Minh Long", "11/12/1967");
        danhSachBaoVe.set(5, longBaoVe);
        
        // Kiêm tra đối tượng có tồn tại trong danh sách không
        boolean contain = danhSachMaNhanVien.contains(5);
        if (danhSachBaoVe.contains(nhatBaoVe))
            System.out.println("Danh sach co chua bao ve Nhat");
        
        
        // xoa theo đối tượng
        danhSachBaoVe.remove(longBaoVe);
        // Xóa theo index (vị trí)
        danhSachBaoVe.remove(5);
        
        for (int i = 0; i< danhSachBaoVe.size(); i++){
            BaoVe aBaoVe = danhSachBaoVe.get(i);
            System.out.println("Bao ve :" + aBaoVe.getMaNhanVien());
        }
        
        for(BaoVe aBaoVe : danhSachBaoVe){
            System.out.println("Bao ve :" + aBaoVe.getMaNhanVien());
        }
        
        danhSachBaoVe.forEach((bv)-> {
            System.out.println("Bao ve :" + bv.getMaNhanVien());
        });
    }
}
