/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vn.fpt.ngocvm.java.ngocvm.c2.b8;

/**
 *
 * @author ngocvm
 */
class AnoCal implements Calculator {

    @Override
    public int cal(int a, int b) {
        return a+b;
    }
}

class AnoMultiCal implements Calculator{

    @Override
    public int cal(int a, int b) {
        return a * b * 2;
    }
    
}

public class Runner {

    public static void main(String[] args) {

        Calculator cc = new AnoCal();
        System.out.println("Ket qua:" + cc.cal(2,3));
  
        
        Calculator c2 = new Calculator() {
            @Override
            public int cal(int a, int b) {
                return a +b;
            }
        };
        System.out.println("Ket qua 2:" + c2.cal(2,3));
        
        
        Calculator c3 = (int a, int b) -> {
                return a +b;
            };
        System.out.println("Ket qua 3:" + c3.cal(2,3));
        
        
        Calculator c4 = (int a, int b) -> a +b;
        System.out.println("Ket qua 4:" + c4.cal(2,3));
        
        
        Calculator c5 = (a, b) -> a +b;
        System.out.println("Ket qua 5:" + c5.cal(2,3));
    }
}
