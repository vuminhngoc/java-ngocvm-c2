package vn.fpt.ngocvm.java.ngocvm.c2.b11;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * requite java 11 (java 7+)
 *
 * @author User-Admin
 */
public class TextFile {

    private final String pathToFile;

    public TextFile(String fileName) {
        this.pathToFile = fileName;
    }
    

    /**
     * Ghi 1 dòng dữ liệu vào file
     *
     * @param oneLine
     */
    public void writeByNio(String oneLine) {
        try {
            // java 8: sử dụng Paths.get() 
            Files.writeString(Path.of(pathToFile), oneLine, 
                    StandardOpenOption.CREATE, // Tạo file nếu chưa có
                    StandardOpenOption.TRUNCATE_EXISTING); // Ghi đè nội dung lên file cũ
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Ghi nhiều dòng dữ liệu, Mỗi string được ghi trên từng dòng khác nhau. Dữ
     * liệu ghi vào được định dạng là UTF-8. Tức là dữ liệu có dấu.
     *
     * @param lines
     * @throws IOException
     */
    public void writeByNio(List<String> lines) throws IOException {
        Files.write(Path.of(pathToFile), lines, 
                StandardCharsets.UTF_8, // Sử dụng chuỗi ký tự có dấu
                StandardOpenOption.CREATE, // tạo file nếu chưa có
                StandardOpenOption.APPEND); // Nếu có tiếp tục ghi tiếp vào file
    }

    /**
     * Sử dụng biện pháp cơ bản để ghi 1 dòng ra file.
     *
     * @param oneLine
     */
    public void writeByBufferedWriter(String oneLine) {
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(pathToFile, true));
            writer.append(oneLine);
            writer.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    
    /**
     * Ghi nhiều dòng sử dụng phương pháp cơ bản. Tuy nhiên sử dụng cấu trúc
     * mới của java 8: try-with-resource, sẽ tự động đóng khi kết thúc quá trình
     * sử dụng.
     * @param lines 
     */
    public void writeByBufferedWriter(List<String> lines) {

        try (BufferedWriter writer = new BufferedWriter(new FileWriter(pathToFile, true))) {
            for (String line : lines) {
                writer.append(line + "\n");
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    
    /**
     * Đọc dữ liệu từ file và trả lại toàn bộ nội dung thành 1 String duy nhất.
     * @return nội dung file
     */
    public String readFromFileUsingNIO(){
        try {
            return Files.readString(Path.of(pathToFile));
        } catch (IOException ex) {
            return null;
        }
    }
    
    
    /**
     * Đọc dữ liệu từ file và trả lại toàn bộ nội dung, mỗi dòng là 1
     * phần tử của danh sách.
     * @return nội dung file theo từng dòng.
     */
    public List<String> readLinesFromFileUsingNIO(){
        try {
            return Files.readAllLines(Path.of(pathToFile));
        } catch (IOException ex) {
            return null;
        }
    }
    
    /**
     * Đọc nội dung của file sử dụng scanner, và đọc theo từng dòng.
     * @return 
     */
    public List<String> readFileUsingScanner(){
        List<String> lines = new ArrayList<>();
        try {
            Scanner scnr = new Scanner(new File(pathToFile));
            int lineNumber = 1;
            while(scnr.hasNextLine()){
                String line = scnr.nextLine();
                lines.add(line);
                lineNumber++;
            }
        } catch (FileNotFoundException ex) {
            // bỏ qua
        }
        return lines;
    }
    

    public static void main(String[] args) {
        TextFile file = new TextFile("datafile.txt");
        
        String oneLine = "Khoa hoc java co ban";
        
        List<String> multiLine = List.of("Multi line at 1", "Multi line at 2", 
                "Multi line at 3", "Multi line at 4", "Multi line at 5 (Dòng số 5)");
        
        file.writeByNio(oneLine);
        // Do trong phương thức không xử lý ngoại lệ lên khi gọi cần phải
        // try catch hoặc tiếp tục ném
        try {
            file.writeByNio(multiLine);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        file.writeByBufferedWriter(oneLine);
        file.writeByBufferedWriter(multiLine);
        
        
    }
}
