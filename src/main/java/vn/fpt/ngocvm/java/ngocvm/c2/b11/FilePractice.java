/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vn.fpt.ngocvm.java.ngocvm.c2.b11;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ngocvm
 */
public class FilePractice {
    public static void main(String[] args) {
        try {
            String content = "Dây chuyền máy rung và sấy sản phẩm"; 
            Files.writeString(Path.of("dat\\datajava2.txt"), content,
                    StandardOpenOption.CREATE, // Tạo file nếu chưa có
                    StandardOpenOption.TRUNCATE_EXISTING); // Ghi đè nội dung lên file cũ
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
