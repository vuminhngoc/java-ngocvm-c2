/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vn.fpt.ngocvm.java.ngocvm.c2.b6;

/**
 *
 * @author User-Admin
 */
public class NhanVienVanPhong extends NhanVien{
    protected String totNghiepTruong;
    protected String queQuan;

    public NhanVienVanPhong(String totNghiepTruong, String queQuan, int maNhanVien, String tenNhanVien, String ngaySinh) {
        super(maNhanVien, tenNhanVien, ngaySinh);
        this.totNghiepTruong = totNghiepTruong;
        this.queQuan = queQuan;
    }

    public String getTotNghiepTruong() {
        return totNghiepTruong;
    }

    public void setTotNghiepTruong(String totNghiepTruong) {
        this.totNghiepTruong = totNghiepTruong;
    }

    public String getQueQuan() {
        return queQuan;
    }

    public void setQueQuan(String queQuan) {
        this.queQuan = queQuan;
    }

    
}
