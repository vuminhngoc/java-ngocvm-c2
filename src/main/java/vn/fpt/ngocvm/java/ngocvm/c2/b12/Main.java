/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vn.fpt.ngocvm.java.ngocvm.c2.b12;
import javax.swing.*;

public class Main {
    public static void main(String[] args) {
        JFrame frame = new JFrame("Main Frame");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JButton showDialogButton = new JButton("Show Dialog");
        showDialogButton.addActionListener(e -> {
            JDialog dialog = new JDialog(frame, "Dialog", true);
            dialog.setSize(200, 200);
            dialog.setVisible(true);
        });

        frame.getContentPane().add(showDialogButton);
        frame.setSize(300, 300);
        frame.setVisible(true);
    }
}