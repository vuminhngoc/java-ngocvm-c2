/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vn.fpt.ngocvm.java.ngocvm.c2.b9;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ngocvm
 */
public class Simple {

    public static void main(String[] args) {
        List<Integer> danhSachSo = new ArrayList<>();
        danhSachSo.add(3);
        danhSachSo.add(4);
        danhSachSo.add(8);
        danhSachSo.add(1);
        
        Integer removeObj = 8;
        int removeIndex = 8;
        danhSachSo.remove(removeObj);

        System.out.println("Toan bo danh sach:" + danhSachSo);

        for (int i = 0; i < danhSachSo.size(); i++) {
            if (danhSachSo.get(i) % 2 == 0) {
                System.out.println(" - " + danhSachSo.get(i));
            }
        }

        for (Integer so : danhSachSo) {
            if (so % 2 == 0) {
                System.out.println(" - " + so);
            }
        }

        
        int x = 5;
        if (danhSachSo.contains(x))
            System.out.println("X co ton tai trong danh sach");
        else
             System.out.println("X khong ton tai trong danh sach");
        
        

//        int i = 5;float c = 0.5f; double f = 0.6d;
//        System.out.println("Gia tri cua i=" + i);
//        
//        Integer j = 5;
//        Integer k = 6;
//        Float cc = 0.5f;
//        System.out.println("Gia tri cua j="+(j+k));
    }
}
