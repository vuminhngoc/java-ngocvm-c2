/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vn.fpt.ngocvm.java.ngocvm.c2.b12;

/**
 *
 * @author User-Admin
 */
public class ThreadExample2 {

    public static void main(String[] args) {
        System.out.println("Main thread được bắt đầu");

        for (int i = 0; i < 100; i++) {
            try {
                System.out.println("MAIN thread - " + i);
                Thread.sleep(100);

                if (i == 5) {
                        try {
                            for (int j = 5; j < 105; j++) {
                                System.out.println(" - > NHANH thread :: " + j);
                                Thread.sleep(100);
                            }
                        } catch (InterruptedException ex) {
                            // pass
                        }
                }
            } catch (InterruptedException ex) {
                // pass
            }
        }
    }
}

