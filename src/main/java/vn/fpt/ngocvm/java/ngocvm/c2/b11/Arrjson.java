/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vn.fpt.ngocvm.java.ngocvm.c2.b11;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonParser;
import java.util.List;

/**
 *
 * @author User-Admin
 */
public class Arrjson {
    public static void main(String[] args) {
        Gson gson = new Gson();

        // Chuyển đổi từ json obj thành chuỗi json
        List<String> lsArr = List.of("String a", "String b", "String c");
        String json = gson.toJson(lsArr);
        System.out.println("json string:" + json);

        
        // Đọc ngược lại từ chuỗi json về dạng java obj
        List<String> convertFromJson = gson.fromJson(json, List.class);
        System.out.println("Arr of convert = " + convertFromJson);

        // Có thể parse json theo id
        JsonArray jsonArr = JsonParser.parseString(json).getAsJsonArray();
        jsonArr.forEach((jsonElement)-> 
                System.out.println("Element:"+jsonElement.getAsString()));
        
    }
}
