/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vn.fpt.ngocvm.java.ngocvm.c2.b7;

import java.util.ArrayList;

/**
 *
 * @author ngocvm
 */
public class Runner {

    public static void main(String[] args) {
        IRandomer useClassRandom = new UseClassRandom();
        System.out.println("Gia tri random tu class random: "
                + useClassRandom.getRandomNumber(200));

        IRandomer useSystemTime = new SystemTimeRandom();
        System.out.println("Gia tri random tu system time: "
                + useSystemTime.getRandomNumber(200));

        IRandomer useSystemTime2 = (int maxValue) -> {
            System.out.println("abc 1 2 3");
            return (int) (System.currentTimeMillis() % maxValue);
        };
        
        System.out.println("Gia tri random tu Anonymus Class: "
                + useSystemTime2.getRandomNumber(200));
    }
}
