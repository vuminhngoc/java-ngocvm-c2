/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package vn.fpt.ngocvm.java.ngocvm.c2.b8;

/**
 *
 * @author ngocvm
 */
public interface Calculator {
    
    public int cal(int a, int b);
}
