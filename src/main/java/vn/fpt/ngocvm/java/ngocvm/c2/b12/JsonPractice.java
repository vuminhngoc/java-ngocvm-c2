/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vn.fpt.ngocvm.java.ngocvm.c2.b12;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import vn.fpt.ngocvm.java.ngocvm.c2.b6.BaoVe;

/**
 *
 * @author ngocvm
 */
public class JsonPractice {
    public static void main(String[] args) {
        // Tạo 4 đối tượng bảo vệ và add vào Map
        BaoVe baove1 = new BaoVe("Dai den", 3, "Thanh", "20/03/1987");
        BaoVe baove2 = new BaoVe("Dai do", 15, "Tuan", "20/04/1981");
        BaoVe baove3 = new BaoVe("Dai vang", 21, "Xuan", "20/05/1997");
        BaoVe baove4 = new BaoVe("Dai xanh", 333, "Vinh", "20/06/1967");
        
        Map<Integer, BaoVe> map = new HashMap<>();
        map.put(baove1.getMaNhanVien(), baove1);
        map.put(baove2.getMaNhanVien(), baove2);
        map.put(baove3.getMaNhanVien(), baove3);
        map.put(baove4.getMaNhanVien(), baove4);
        
        baove1.getNgaySinh();
        
        Gson gson = new Gson();
        
        String baove1Json = gson.toJson(baove1);
        System.out.println(baove1Json);
        
        Gson gsonDep = new GsonBuilder().setPrettyPrinting().create();
        
        baove1Json = gsonDep.toJson(map);
        System.out.println(baove1Json);
        
        List<BaoVe> lsBaoVe = new ArrayList<>(map.values());
        String lsBaoVeJSON = gsonDep.toJson(lsBaoVe);
        System.out.println(lsBaoVeJSON);
    }
}
