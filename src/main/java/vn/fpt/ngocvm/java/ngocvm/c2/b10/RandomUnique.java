/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vn.fpt.ngocvm.java.ngocvm.c2.b10;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

/**
 * Yêu cầu: Tạo ra danh sách 10000 phần tử số nguyên không trùng nhau, và phần
 * tử đó nằm trong khoảng từ 0 - 30000
 *
 * @author ngocvm
 */
public class RandomUnique {

    public static void main(String[] args) {
        List<Integer> danhSach = new ArrayList<>();
        Set<Integer> tapHop = new HashSet<>();

        Random rand = new Random();

        // Đánh dấu thời gian bắt đầu xử lý
        long startTime = System.currentTimeMillis();

        while (danhSach.size() <= 100000) {
            int randNum = rand.nextInt(300000);
            if (!danhSach.contains(randNum)) {
                danhSach.add(randNum);
            }
        }

        System.out.println(danhSach);
        System.out.println("Thoi gian xy ly ms=" + (System.currentTimeMillis() - startTime));

        // Tap hop
        startTime = System.currentTimeMillis();
        while (tapHop.size() <= 100000) {
            int randNum = rand.nextInt(300000);
            tapHop.add(randNum);
        }
        
        System.out.println(tapHop);
        System.out.println("Thoi gian xy ly tap hop ms=" + (System.currentTimeMillis() - startTime));
    }
}
