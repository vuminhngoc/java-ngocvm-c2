/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vn.fpt.ngocvm.java.ngocvm.c2.b9;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import vn.fpt.ngocvm.java.ngocvm.c2.b6.BaoVe;
import vn.fpt.ngocvm.java.ngocvm.c2.b6.NhanVien;
import vn.fpt.ngocvm.java.ngocvm.c2.b6.WorkBenefit;

/**
 *
 * @author User-Admin
 */
public class ExampleHashset {
    public static void main(String[] args) {
        
        // Khởi tạo tập hợp tapHopMaNhanVien với kích thước ban đầu là 500
        // và chỉ chứa các số nguyên
        Set<Integer> tapHopMaNhanVien = new HashSet<>(500);
        // Khởi tạo set tapHopBaoVe, là một danh sách với kiểu là đối 
        // tượng thuộc class BaoVe
        Set<BaoVe> tapHopBaoVe = new HashSet<>();
        // Khởi tạo set tapHopNhanVien, là một danh sách với kiểu là đối 
        // tượng thuộc class cha NhanVien. Dữ liệu được lấy từ tapHopBaoVe
        Set<NhanVien> tapHopNhanVien = new HashSet<>(tapHopBaoVe);
        // Khởi tạo set tapHopWorkBenefit, là một danh sách với kiểu là đối 
        // tượng thuộc interface WorkBenefit. Dữ liệu được lấy từ tapHopBaoVe
        Set<WorkBenefit> tapHopWorkBenefit = new HashSet<>(tapHopBaoVe);
        
        
        tapHopMaNhanVien.add(1);
        int maNhanVienA = 56;
        tapHopMaNhanVien.add(maNhanVienA);
        
        BaoVe nhatBaoVe = new BaoVe("Dai Den taekwondo", 2, "Nguyễn Văn Nhật", "10/10/1977");
        tapHopBaoVe.add(nhatBaoVe);
        tapHopNhanVien.add(nhatBaoVe);
        tapHopWorkBenefit.add(nhatBaoVe);
        
        
        BaoVe longBaoVe = new BaoVe("Dai Do taekwondo", 3, "Nguyễn Minh Long", "11/12/1967");
        tapHopBaoVe.add( longBaoVe);
        
        // Kiêm tra đối tượng có tồn tại trong danh sách không
        boolean contain = tapHopMaNhanVien.contains(5);
        if (tapHopBaoVe.contains(nhatBaoVe))
            System.out.println("Danh sach co chua bao ve Nhat");
        
        tapHopMaNhanVien.remove(213245);
        tapHopBaoVe.remove(longBaoVe);
        
        for(BaoVe aBaoVe : tapHopBaoVe){
            System.out.println("Bao ve :" + aBaoVe.getMaNhanVien());
        }
        
        tapHopBaoVe.forEach((bv)-> {
            System.out.println("Bao ve :" + bv.getMaNhanVien());
        });

    }
}
