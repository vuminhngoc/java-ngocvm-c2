/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vn.fpt.ngocvm.java.ngocvm.c2.b11;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 *
 * @author User-Admin
 */
class Address {

    String street;
    List<String> zipCodes;
}

class DataObject {

    int id;
    String name;
    int[] score;
    List<String> courses;
    Map<Integer, String> mapData;
    Address addr;
}

public class Json {

    public static DataObject createSampleDataObj() {
        Address address = new Address();
        address.street = "Hoa lac";
        address.zipCodes = List.of("zip.500", "zip.578", "zip.674");

        DataObject object = new DataObject();
        object.id = 1;
        object.name = "Sample";
        object.score = new int[3];
        object.score[0] = 5;
        object.score[1] = 10;
        object.score[2] = 4;

        object.courses = List.of("course.1", "course.2", "course.3");
        object.mapData = Map.of(1, "data string 1", 2, "data string 2", 3, "data string 3");
        object.addr = address;
        return object;
    }

    public static void main(String[] args) {
        Gson gson = new Gson();

        // Chuyển đổi từ json obj thành chuỗi json
        DataObject obj = createSampleDataObj();
        String json = gson.toJson(obj);
        System.out.println("json string:" + json);

        Gson gson2 = new GsonBuilder().setPrettyPrinting().create();
        json = gson2.toJson(obj);
        System.out.println(json);

        // Đọc ngược lại từ chuỗi json về dạng java obj
        DataObject convertFromJson = gson.fromJson(json, DataObject.class);
        System.out.println("Street of convert = " + convertFromJson.addr.street);

        // Có thể parse json theo id
        JsonObject jsonObject = JsonParser.parseString(json).getAsJsonObject();
        JsonElement id = jsonObject.get("id");
        JsonElement name = jsonObject.get("name");
        System.out.println( "id = " + id.getAsBigInteger());
        System.out.println( "Name = " + name.getAsString());
        
    }
}
