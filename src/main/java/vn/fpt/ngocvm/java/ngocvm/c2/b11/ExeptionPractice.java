/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vn.fpt.ngocvm.java.ngocvm.c2.b11;

/**
 *
 * @author ngocvm
 */
public class ExeptionPractice {

    public static void main(String[] args) {
        try {
            int[] arr = {1, 2, 5};
            System.out.println("pt = " + arr[1]);
            for (int i : arr) {
                System.out.println("5/i" + (5 / i));
                if (i % 2 == 1)
                    return;
            }
        } catch (ArrayIndexOutOfBoundsException e) {
             System.out.println("Xử lý phần ngoài mảng");
        } catch (Exception e){
            System.out.println("Xử lý các lỗi khác");
        } finally{
            System.out.println("finally");
        }
            
    }
}
