/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vn.fpt.ngocvm.java.ngocvm.c2.b10;

import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author ngocvm
 */
public class ToaDo{
    public int x;
    public int y;

    public ToaDo(int x, int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public String toString() {
        return "ToaDo{" + "x=" + x + ", y=" + y + '}';
    }

    @Override
    public boolean equals(Object obj) {
        ToaDo o = (ToaDo) obj;
        
        if(this.x == o.x && this.y == o.y)
            return true;
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + this.x;
        hash = 97 * hash + this.y;
        return hash;
    }

    public static void main(String[] args) {
        ToaDo p1 = new ToaDo(4, 5);
        ToaDo p2 = p1;
        ToaDo p3 = new ToaDo(4, 7);
        
        Object obj = new ToaDo(0, 0);
        
        System.out.println("P1="+ p1);
        System.out.println("P2="+ p2);
        System.out.println("P3="+ p3);
        
        if (p1.equals(p2))
            System.out.println("P1 = P2");
        
        if (p1.equals(p3))
           System.out.println("P1 = P3"); 
        
        Set<ToaDo> tapHopToaDo = new HashSet<>();
        tapHopToaDo.add(p1);
        tapHopToaDo.add(p2);
        tapHopToaDo.add(p3);
        System.out.println("Kich thuoc cua Tap toa do="+tapHopToaDo);
        
        int i = 5;
        int j = i;
        j = 6;
        System.out.println("i =" + i);
        
        Set<ToaDo> tapHopToaDo2 = tapHopToaDo;
        tapHopToaDo2.remove(new ToaDo(4, 5));
        
        System.out.println("Kich thuoc cua Tap toa do 1="+tapHopToaDo);
        System.out.println("Kich thuoc cua Tap toa do 2="+tapHopToaDo2);
    }
}
