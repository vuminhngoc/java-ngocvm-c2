/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vn.fpt.ngocvm.java.ngocvm.c2.b7;

import java.util.ArrayList;

/**
 *
 * @author ngocvm
 */
public class Dog {

    int id;
    String name;

    public Dog setId(int id) {
        this.id = id;
        return this;
    }

    public Dog setName(String name) {
        this.name = name;
        return this;
    }

    @Override
    public String toString() {
        return "Dog{" + "id=" + id + ", name=" + name + '}';
    }

    public static void main(String[] args) {
        Dog a = new Dog();

        a.setId(5);
        a.setName("Dat");

        System.out.println("a=" + a);

        a.setId(6).setName("Dat b");
        System.out.println("a=" + a);
        
        ArrayList b = new ArrayList();
        b.iterator().equals(a);
    }
}
