/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vn.fpt.ngocvm.java.ngocvm.c2.b6;

/**
 *
 * @author User-Admin
 */
public class NhanVienKho extends NhanVienVanPhong implements WorkBenefit{
    private String tinhTrangSucKhoe;

    public NhanVienKho(String tinhTrangSucKhoe, String totNghiepTruong, String queQuan, int maNhanVien, String tenNhanVien, String ngaySinh) {
        super(totNghiepTruong, queQuan, maNhanVien, tenNhanVien, ngaySinh);
        this.tinhTrangSucKhoe = tinhTrangSucKhoe;
    }

    public String getTinhTrangSucKhoe() {
        return tinhTrangSucKhoe;
    }

    public void setTinhTrangSucKhoe(String tinhTrangSucKhoe) {
        this.tinhTrangSucKhoe = tinhTrangSucKhoe;
    }

    @Override
    public String getWorkBenefit() {
        return "Chi tra chi phi kham & chua benh.";
    }
    
}
