/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package vn.fpt.ngocvm.java.ngocvm.c2.b7;

/**
 *
 * @author ngocvm
 */
public interface IRandomer {
    /**
     * Trả lại giá trị ngẫu nhiên từ 0 đến maxValue.
     * @param maxValue : Giá trị cao nhất (không bao gồm)
     * @return 
     */
    public int getRandomNumber(int maxValue);
}
